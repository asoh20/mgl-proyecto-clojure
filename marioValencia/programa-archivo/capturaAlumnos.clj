(import '(java.util Scanner))
(use 'clojure.java.io)
(use '[clojure.string :only (join split)])

(def incremento 0) 
(def v [])
(def scan (Scanner. *in*))
(def rutaArchivo "")

(defn Leer[x]
	(with-open [rdr (reader (str "/Users/mario/repositorio_codigo/mgl-proyecto-clojure/marioValencia/file_example/" x))]
  		(doseq [line (line-seq rdr)]
     		(println (join "|"(split line #",")))
     		;(def linea (str line "\n"))
     		(def v (assoc v incremento line))
     		 (def incremento (+ incremento 1))
     		
     	) 
 	)

)


(defn registrarAlumno[ruta arreglo] 
(with-open [wrtr(writer (str "/Users/mario/repositorio_codigo/mgl-proyecto-clojure/marioValencia/file_example/" ruta ))]
(println "esto se sobrescribira: " arreglo)
(def lim (count arreglo))
(println lim)
(def i 0)
(while (< i lim)
	(println (nth arreglo i))
	(.write wrtr (str (nth arreglo i) "\r\n"))
	(def i (+ i 1))
	)
)
)

(defn agregarAlumno[ruta] 
	(println "introduce el numero de control ")
	(def noControl(.next scan))
	(println "introduce el nombre del alumno: ")
	(def nombre(.next scan))
	(println "introduce la carrera ")
	(def carrera(.next scan))
	(println "introduce el semestre ")
	(def semestre(.next scan))
	(println "Sexo")
	(def sexo(.next scan))

    (def mapaAlumno {:mNumero noControl :mNombre nombre :mCarrera carrera :mSemestre semestre :mSexo sexo})
    (def datos (str (get mapaAlumno :mNumero)","(get mapaAlumno :mNombre)","(get mapaAlumno :mCarrera)","(get mapaAlumno :mSemestre)","(get mapaAlumno :mSexo)))
    (def v (assoc v incremento datos))
    (registrarAlumno ruta v)
)

(defn eleccionTarea[x] 
	(if (= x "1") 
		(agregarAlumno rutaArchivo)
		(if (= x "2") (Leer rutaArchivo) (println "Salir"))
    )
)

(println "Introduce la ruta del archivo txt omitiendo C:/ ")
(def rutaArchivo(.next scan))

(println " ")
(println "Alumnos registrados")
(println " ")
(Leer rutaArchivo)
(println " ")
(println "Bienvenido al sistema de registro de alumnos")
(println "Menu")
(println "1) Agregar un nuevo Alumno")
(println "2) Mostrar Alumnos registrados")
(println "3) Salir")
(println " ")
(def eleccion(.next scan))
(eleccionTarea eleccion)
