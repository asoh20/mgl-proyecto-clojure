(ns view.vista  )
(import '(java.util Scanner))
(def scan (Scanner. *in*))

(println "Leer 1)  -----  Agregar 2)")
(def opcion (.nextLine scan))
(def agregar "1")
(def leer "2")


(case opcion
  "1" (def a 1)
  "2" (def a 2)
  (str "else" opcion(println "opcion no valida") (def a 3)) 
)


(defn vista []

	(+ 0 a)
	
	)

(defn text []
(println "--Marca--Modelo--Color--No. de Pasajeros--Maxima Velocidad--") 
(println "") 

    (println "Introduce la marca del coche") 
	(def marca (.nextLine scan))
	(println "Introduce el modelo del coche") 
	(def modelo (.nextLine scan))
	(println "Introduce la color del coche") 
	(def color (.nextLine scan))
	(println "Introduce el numero de pasajeros") 
	(def pasajeros (.nextLine scan))
	(println "Introduce la velocidad maxima del coche") 
	(def velocidad (.nextLine scan))

	(str marca " , " modelo " , " color " , " pasajeros " , " velocidad)

   ;;(def datos [marca modelo color pasajeros velocidad])

)
