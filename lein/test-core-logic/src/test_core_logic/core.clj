(ns test-core-logic.core
  (:gen-class)
  (:refer-clojure :exclude [==])
  (:use [clojure.core.logic]))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]

  	(println "Hello, World! mike")

	(println (run* [q]
	  (== 1 1))
	)
	

	(println "****************")

	(println (run* [q]
	  (== q 1)))


	(println "****************")


(defrel father Father Child)
(defrel mother Mother Child)
 
(facts father [['Vito 'Michael]
               ['Vito 'Sonny]
               ['Vito 'Fredo]
               ['Michael 'Anthony]
               ['Michael 'Mary]
               ['Sonny 'Vicent]
               ['Sonny 'Francesca]
               ['Sonny 'Kathryn]
               ['Sonny 'Frank]
               ['Sonny 'Santino]])
 
(facts mother [['Carmela 'Michael]
               ['Carmela 'Sonny]
               ['Carmela 'Fredo]
               ['Kay 'Mary]
               ['Kay 'Anthony]
               ['Sandra 'Francesca]
               ['Sandra 'Kathryn]
               ['Sandra 'Frank]
               ['Sandra 'Santino]])

(println (run* [q]
  (father 'Vito q))
)


)