(import '(java.util Scanner))
(println  "Introduce el monto a calcular: " )
(def scan (Scanner. *in*))
(def Monto(.nextInt scan))

(if(> Monto 100) 
(do
	(def desc(/ (* Monto 10) 100))
	(print "el monto con el 10% es: " desc)
	)  
(do
	(def desc2(/ (* Monto 2) 100))
	(print "el monto con el 2% es: " desc2)
	) )


