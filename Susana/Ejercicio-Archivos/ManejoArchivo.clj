(import '(java.util Scanner))
(use 'clojure.java.io) 
(use '[clojure.string :only (join split)])
(def scan (Scanner. *in*))
(def op1 "1")
(def op2 "2")
(def computadora "")

(println "---MANEJO  DE ARCHIVOS---")
(println "--OPCIONES--")
(println "1) Leer el Archivo")
(println "2) Agregar al Archivo")

(def opcion (.next scan))

(defn leerA []
	(with-open [rdr (reader "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt" :append true)]
  		(doseq [line (line-seq rdr)]
     		(println(join "|"(split line #","))) ))
)

(if (.exists (as-file "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt"))
		(println "Archivo existente")
		(spit "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt" "")
	)

(defn agregarCompu []
	(def computadora (.next scan))
	(with-open [wrtr (writer "C:\\Users\\SUSANA\\repositorio_codigo\\mgl-proyecto-clojure\\Susana\\Ejercicio-Archivos\\archi.txt" :append true)](.write wrtr computadora) (.newLine wrtr))
)

(if (= opcion op1) 
	(do 
	(println "--Computadoras agregadas--")
		(leerA) )
	)
(if (= opcion op2)
	(do 
		(println "Opcion 2")
			(println "Escribir en el siguiente orden: ")
			(println "  Procesador,RAM,DD,SO,Tipo Computadora")
			(agregarCompu)
		)
	)