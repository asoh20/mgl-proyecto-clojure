(import '(java.util Scanner))
(def teclado (Scanner. *in*))
(def descuento 0)
(def montoD 0)
(def div 0)

(println "---DESCUENTO SEGUN EL MONTO---")
(println "Ingrese el monto")
(def monto (.nextInt teclado))

(if (<= monto 100)
	(def descuento (* monto 0.02))

	(if(> monto 100)
		;(def div (/ 10 100))
		(def descuento (* monto 0.10))
	)
)

(def montoD (- monto descuento))

(println "El monto es:")
(println monto)
(println "El descuento es:")
(println descuento)
(println "El monto con descuento es:")
(println montoD)