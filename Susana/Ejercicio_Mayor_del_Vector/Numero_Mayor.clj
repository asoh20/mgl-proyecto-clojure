(def v (vector 2 5 8 1 10))
(println "EJERCICIO N° 17")
(println "NUMERO MAYOR DEL VECTOR")
(println "vector:" v)
(def tam 4)
(def NumeroMayor 0)
(println "Calculando...")

(defn Funmayor [v tam] ; <----inicio de la funcion
(def numComparativo (nth v tam))

(if (= tam 0) (do
	(if (> numComparativo NumeroMayor)
		(def NumeroMayor numComparativo)
		)
	
	(println "El Mayor es:" NumeroMayor)
)

(if (> numComparativo NumeroMayor)
	(do 
		(def NumeroMayor numComparativo)
		(def tam (- tam 1))
		(Funmayor v tam)
		)
	(do
		(def t (- tam 1))
		(Funmayor v t) 
		)
	)

	);<---- parantesis del primer if
);<----parentesis de la funcion

(Funmayor v tam)