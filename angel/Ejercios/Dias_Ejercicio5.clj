(import '(java.util Scanner))
(println  "=> Introduce Tiempo en minutos:")
(def scan (Scanner. *in*))  
(def  tiempo(.nextInt scan))


(if(>= (mod tiempo 1440) 0)
(do
	(def ds(quot tiempo 1440))
	(def x(mod tiempo 1440))
	(def horas(quot x 60))
	(def minutos(mod x  60))

	(print ds "dias," horas "hrs., "  minutos " min." )
	) 
) 
